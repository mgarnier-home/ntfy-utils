import dotenv from 'dotenv';
//the config is coming from a file name config.json
import Fs from 'node:fs';
import Path from 'node:path';

import { Config } from './interfaces.js';

dotenv.config();

const loadConfigFromEnv = (): Config => {
  const config: Config = {
    ntfyProtocol: process.env.NTFY_PROTOCOL || 'http',
    ntfyTopic: process.env.NTFY_TOPIC || '',
    ntfyServer: process.env.NTFY_SERVER || '',
  };

  return config;
};

export const config = loadConfigFromEnv();
